#include <boost/program_options.hpp>
#include <iostream>

#include "Config.hpp"
#include "Daemon.hpp"
#include "dlexception.hpp"

namespace po = boost::program_options;

int main (int argc, char *argv[])
{
	// Declare the supported options.
	try {
		po::options_description desc("Allowed options");
		desc.add_options()
		("help,h", "produce help message")
		("help-module", po::value<std::string>(), "produce help message for a specific module")
		("version,v", "print version string")
		("modules,m", po::value<std::vector<std::string> >()->multitoken(), "list of modules to load")
		;

		po::positional_options_description pos_desc;

		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).options(desc).positional(pos_desc).allow_unregistered().run(), vm);
		po::notify(vm);

		if (vm.count("help")) {
			std::cout << desc << std::endl;
			return 1;
		}

		if (vm.count("help-module")) {
			Daemon::Instance()->addModule(vm["help-module"].as<std::string>());
			std::cout << Daemon::Instance()->getOptions() << std::endl;
			return 1;
		}

		if (vm.count("version")) {
			std::cout << "daemon404 verion: " << Daemon404_VERSION_MAJOR << "." <<
			Daemon404_VERSION_MINOR << std::endl;
			return 1;
		}

		if (vm.count("modules")){
			for (const std::string &s : vm["modules"].as<std::vector<std::string>>()){
				Daemon::Instance()->addModule(s);
			}
		}

		desc.add(Daemon::Instance()->getOptions());

		po::store(po::command_line_parser(argc, argv).options(desc).positional(pos_desc).run(), vm);
		po::notify(vm);

		Daemon::Instance()->handleOptions(vm);
	}
	catch(po::error& e) {
		std::cerr << "error: " << e.what() << std::endl;
		std::cerr << "Use --help to get info on how to use daemon404." << std::endl;
		return 1;
	} catch(dlexception& e) {
		std::cerr << "error: " << e.what() << std::endl;
		return 1;
	} catch (std::logic_error& e){
		std::cerr << "error: " << e.what() << std::endl;
		return 1;
	}

	try {
		return Daemon::Instance()->start();
	} catch(dlexception& e) {
		std::cerr << "error: " << e.what() << std::endl;
		return 1;
	}
	std::cout << "exiting" << std::endl;
}