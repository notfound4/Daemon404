#include <cstdlib>
#include <cstring>
#include <iostream>
#include <boost/asio.hpp>

using boost::asio::local::stream_protocol;

constexpr std::size_t max_length = 1024;

int main (int argc, char *argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Usage: stream_client message" << std::endl;
      return 1;
    }

    if (std::string(argv[1]).empty()) {
      std::cerr << "error: empty command" << std::endl;
      return 1;
    }

    if (getenv("XDG_RUNTIME_DIR") == NULL) {
      std::cerr << "error: cannot find XDG_RUNTIME_DIR" << std::endl;
      return 1;
    }
    std::string runtime_dir = getenv("XDG_RUNTIME_DIR");
    runtime_dir += "/daemon404.socket";

    boost::asio::io_service io_service;

    stream_protocol::socket s(io_service);
    s.connect(stream_protocol::endpoint(runtime_dir.c_str()));

    size_t request_length = std::strlen(argv[1]);
    boost::asio::write(s, boost::asio::buffer(argv[1], request_length));

    char reply[max_length];
    size_t reply_length = s.read_some(boost::asio::buffer(reply, max_length));
    std::cout.write(reply, reply_length);
    std::cout << std::endl;
  }
  catch (std::exception& e)
  {
    std::cerr << "error: " << e.what() << std::endl;
  }

  return 0;
}