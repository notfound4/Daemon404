#include "dlexception.hpp"
#include "ModuleInterface.hpp"
#include "pulseaudio/Sink.hpp"

#include <iostream>
#include <libnotify/notify.h>
#include <list>
#include <memory>
#include <pulse/pulseaudio.h>
#include <sstream>


#ifdef UNUSED
#elif defined(__GNUC__)
# define UNUSED(x) UNUSED_ ## x __attribute__((unused))
#elif defined(__LCLINT__)
# define UNUSED(x) /*@unused@*/ x
#else
# define UNUSED(x) x
#endif

namespace po = boost::program_options;

void conflicting_options(const po::variables_map& vm, 
                         const char* opt1, const char* opt2)
{
    if (vm.count(opt1) && !vm[opt1].defaulted() 
        && vm.count(opt2) && !vm[opt2].defaulted())
        throw std::logic_error(std::string("Conflicting options '") 
                          + opt1 + "' and '" + opt2 + "'.");
}

enum state {
    CONNECTING,
    CONNECTED,
    ERROR
};
typedef enum state state_t;

class ModulePulseaudio : public ModuleInterface {
public:
    ModulePulseaudio() : ModuleInterface(), m_sink(nullptr), m_step(5), m_do_notif(true) {
    	m_mainloop = pa_mainloop_new();
    	m_mainloop_api = pa_mainloop_get_api(m_mainloop);
    	m_context = pa_context_new(m_mainloop_api, "daemon404");
    	pa_context_set_state_callback(m_context, [](pa_context* context, void* raw){
    		ModulePulseaudio* pulse = (ModulePulseaudio*) raw;
    		switch(pa_context_get_state(context))
    		{
    			case PA_CONTEXT_READY:
    			pulse->setState(CONNECTED);
    			break;
    			case PA_CONTEXT_FAILED:
    			pulse->setState(ERROR);
    			break;
    			case PA_CONTEXT_UNCONNECTED:
    			case PA_CONTEXT_AUTHORIZING:
    			case PA_CONTEXT_SETTING_NAME:
    			case PA_CONTEXT_CONNECTING:
    			case PA_CONTEXT_TERMINATED:
    			break;
    		}
    	}, this);

    	m_state = CONNECTING;
    	int retval;
    	if (pa_context_connect(m_context, NULL, PA_CONTEXT_NOFLAGS, NULL) < 0)
    		throw dlopenexception("pulseaudio", "Connection error");
    	while (m_state == CONNECTING) {
    		if (pa_mainloop_iterate(m_mainloop, 1, &retval) < 0)
    			throw dlopenexception("pulseaudio", "Mainloop error");
    	}
    	if (m_state == ERROR)
    		throw dlopenexception("pulseaudio", "Connection error");

    	if (!notify_is_initted ()){
    		notify_init ("daemon404");
    	}

    	m_notif = notify_notification_new ("", "", 0);
    }

    ~ModulePulseaudio() {
    	if (m_state == CONNECTED)
    		pa_context_disconnect(m_context);
    	pa_mainloop_free(m_mainloop);
    }

	virtual po::options_description getOptions() {
		po::options_description desc;

		desc.add_options()
		("pulseaudio-sink-index", po::value<int>(), "Index of the sink to use")
		("pulseaudio-sink-name", po::value<std::string>(), "Name of the sink to use")
		("pulseaudio-step", po::value<int>(), "Step to use for increasing and decreasing volume")
		("pulseaudio-notif", po::value<bool>(), "Enable notification")
		("pulseaudio-folder", po::value<std::string>(),
			"Folder containing icons to display (vol_high-64x64.png, vol_low-64x64.png, vol_medium-64x64.png, vol_off-64x64.png).")
		;

		return desc;
	}

	virtual std::string handleMessage(std::string msg) {
		std::istringstream input(msg);
		std::string token;
		if (!(input >> token))
			return "Failure: no command";
		if (token == "mute" && input.eof()) {
			return set_mute(true) > 0 ? "Success" : "Failure: could not mute";
		} else if (token == "unmute" && input.eof()) {
			return set_mute(false) > 0 ? "Success" : "Failure: could not mute";
		} else if (token == "toggle" && input.eof()) {
			return set_mute(!m_sink->mute) > 0 ? "Success" : "Failure: could not mute";
		} else if (token == "up" && input.eof()) {
			return up_volume() > 0 ? "Success" : "Failure: could not change volume";
		} else if (token == "down" && input.eof()) {
			return down_volume() > 0 ? "Success" : "Failure: could not change volume";
		}
		return "Unknown command";
	}

	virtual void handleOptions(const po::variables_map& vm) {
		conflicting_options(vm, "pulseaudio-sink-index", "pulseaudio-sink-name");
		if (vm.count("pulseaudio-sink-index")) {
			set_sink(vm["pulseaudio-sink-index"].as<int>());
		} else if (vm.count("pulseaudio-sink-name")) {
			set_sink(vm["pulseaudio-sink-name"].as<std::string>());
		} else {
			set_sink(get_default_sink());
		}
		if (vm.count("pulseaudio-step"))
			m_step = vm["pulseaudio-step"].as<int>();
		if (vm.count("pulseaudio-notif"))
			m_do_notif = vm["pulseaudio-notif"].as<bool>();
		if (vm.count("pulseaudio-folder"))
			m_folder = vm["pulseaudio-folder"].as<std::string>();
	}

	void setState(state_t state){
		m_state = state;
	}

private:
	void iterate(pa_operation* op) {
    	int retval;
    	while (pa_operation_get_state(op) == PA_OPERATION_RUNNING) {
    		pa_mainloop_iterate(m_mainloop, 1, &retval);
    	}
    }

    int set_mute(bool mute) {
    	set_sink(m_sink->name);
    	int ret;
    	pa_operation* op = pa_context_set_sink_mute_by_name(m_context, m_sink->name.c_str(), (int) mute,
    		[](pa_context* UNUSED(context), int success, void* raw){
    			int *r = (int*) raw;
    			*r = success;
    		}, &ret);
    	iterate(op);
    	pa_operation_unref(op);
    	update_notif();
    	return ret;
    }

    int up_volume() {
    	set_sink(m_sink->name);
    	int new_perc = m_sink->volume_percent + m_step;
    	if (new_perc > 100)
    		new_perc = 100;

    	pa_volume_t new_vol = PA_VOLUME_NORM * static_cast<double>(new_perc) / 100.0;
    	pa_cvolume* new_cvolume = pa_cvolume_set(&m_sink->volume, m_sink->volume.channels, new_vol);
    	int ret;
    	pa_operation* op = pa_context_set_sink_volume_by_name(m_context, m_sink->name.c_str(), new_cvolume,
    		[](pa_context* UNUSED(context), int success, void* raw){
    			int *r = (int*) raw;
    			*r = success;
    		}, &ret);
    	iterate(op);
    	pa_operation_unref(op);
    	update_notif();
    	return ret;
    }

    int down_volume() {
    	set_sink(m_sink->name);
    	int new_perc = m_sink->volume_percent - m_step;
    	if (new_perc < 0)
    		new_perc = 0;

    	pa_volume_t new_vol = PA_VOLUME_NORM * static_cast<double>(new_perc) / 100.0;
    	pa_cvolume* new_cvolume = pa_cvolume_set(&m_sink->volume, m_sink->volume.channels, new_vol);
    	int ret;
    	pa_operation* op = pa_context_set_sink_volume_by_name(m_context, m_sink->name.c_str(), new_cvolume,
    		[](pa_context* UNUSED(context), int success, void* raw){
    			int *r = (int*) raw;
    			*r = success;
    		}, &ret);
    	iterate(op);
    	pa_operation_unref(op);
    	update_notif();
    	return ret;
    }

    void set_sink(std::string name){
    	Sink *sink = nullptr;
    	pa_operation* op = pa_context_get_sink_info_by_name(m_context, name.c_str(),
    		[](pa_context * UNUSED(c), const pa_sink_info *i, int eol, void *raw) {
    			if (eol != 0) return;

    			Sink** s = (Sink**) raw;
    			*s = new Sink(i);
    		}, &sink);
    	iterate(op);
    	pa_operation_unref(op);

    	if (sink == nullptr) throw dlopenexception("pulseaudio", "The sink doesn't exit");
    	m_sink.reset(sink);
    }

    void set_sink(int index){
    	Sink *sink = nullptr;
    	pa_operation* op = pa_context_get_sink_info_by_index(m_context, index,
    		[](pa_context * UNUSED(c), const pa_sink_info *i, int eol, void *raw) {
    			if (eol != 0) return;

    			Sink** s = (Sink**) raw;
    			*s = new Sink(i);
    		}, &sink);
    	iterate(op);
    	pa_operation_unref(op);

    	if (sink == nullptr) throw dlopenexception("pulseaudio", "The sink doesn't exit");
    	m_sink.reset(sink);
    }

    std::string get_default_sink() {
    	std::string default_sink_name;
    	pa_operation* op = pa_context_get_server_info(m_context, [](pa_context* UNUSED(context), const pa_server_info* i, void* raw){
    		std::string* info = (std::string*) raw;
    		*info = i->default_sink_name;
    	}, &default_sink_name);
    	iterate(op);
    	pa_operation_unref(op);
    	return default_sink_name;
    }

    void update_notif() {
    	if (!m_do_notif)
    		return;

    	set_sink(m_sink->name);

    	char *icon = 0;
    	std::string str;
    	if (!m_folder.empty()) {
    		if (m_sink->mute)
    			str = m_folder + "/vol_off-64x64.png";
    		else{
    			if (m_sink->volume_percent >= 67)
    				str = m_folder + "/vol_high-64x64.png";
    			else if (m_sink->volume_percent > 33)
    				str = m_folder + "/vol_medium-64x64.png";
    			else if (m_sink->volume_percent == 0)
    				str = m_folder + "/vol_off-64x64.png";
    			else
    				str = m_folder + "/vol_low-64x64.png";
    		}
    		icon = &str[0];
    	}

    	if (m_sink->mute) {
    		notify_notification_update (m_notif,
    			"Volume Muted",
    			0,
    			icon);
    	} else {
    		notify_notification_update (m_notif,
    			"Volume",
    			0,
    			icon);
    		notify_notification_set_hint_uint32 (m_notif,
                                     "value",
                                     m_sink->volume_percent);
    	}
    	notify_notification_set_timeout(m_notif, 3000);
    	notify_notification_show(m_notif, 0);
    }

	pa_mainloop* m_mainloop;
    pa_mainloop_api* m_mainloop_api;
	pa_context* m_context;

	state_t m_state;

	std::unique_ptr<Sink> m_sink;
	NotifyNotification *m_notif;

	int m_step;
	bool m_do_notif;
	std::string m_folder;
};


// the class factories

extern "C" ModuleInterface* create() {
    return new ModulePulseaudio;
}

extern "C" void destroy(ModuleInterface* p) {
    delete p;
}