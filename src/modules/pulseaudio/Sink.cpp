#include "pulseaudio/Sink.hpp"

#include <cmath>

Sink::Sink(const pa_sink_info* info) {
    index           = info->index;
    name            = info->name;
    description     = info->description;
    mute            = info->mute == 1;
    setVolume(&(info->volume));
}

void Sink::setVolume(const pa_cvolume* v) {
    volume         = *v;
    volume_avg     = pa_cvolume_avg(v);
    volume_percent = (int) round( (double)volume_avg * 100. / PA_VOLUME_NORM );
}