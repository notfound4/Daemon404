get_directory_name(DIR_NAME)

file(GLOB ${DIR_NAME}_SRC "*.cpp")

add_library(daemon404-${DIR_NAME} SHARED ${${DIR_NAME}_SRC})
#target_link_libraries(daemon404-${DIR_NAME} )