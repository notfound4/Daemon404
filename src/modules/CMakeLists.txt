get_subdirectories(MODULES_LIST)

foreach(MODULE_NAME ${MODULES_LIST})
	add_subdirectory(${MODULE_NAME})
endforeach()