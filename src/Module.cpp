#include "dlexception.hpp"
#include "Module.hpp"

#include <dlfcn.h>

Module::Module(std::string lib_name){
	m_lib = dlopen(lib_name.c_str(), RTLD_LAZY);
	if (!m_lib) {
		throw dlopenexception(lib_name);
	}

	// reset errors
	dlerror();

    // load the symbols
	create_t* creater = (create_t*) dlsym(m_lib, "create");
	const char* dlsym_error = dlerror();
	if (dlsym_error) {
		throw dlsymexception(dlsym_error);
	}
    
    m_destroyer = (destroy_t*) dlsym(m_lib, "destroy");
    dlsym_error = dlerror();
    if (dlsym_error) {
        throw dlsymexception(dlsym_error);
    }

	// create an instance of the class
    m_module = creater();
}

Module::~Module(){
	m_destroyer(m_module);
	dlclose(m_lib);
}

boost::program_options::options_description Module::getOptions() {
	return m_module->getOptions();
}

std::string Module::handleMessage(std::string msg) {
	return m_module->handleMessage(msg);
}

void Module::handleOptions(const boost::program_options::variables_map& vm) {
	return m_module->handleOptions(vm);
}

ModuleInterface *Module::get() {
	return m_module;
}