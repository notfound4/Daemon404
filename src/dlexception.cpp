#include "dlexception.hpp"

const char* dlexception::what() const throw()
{
	return "";
}

dlopenexception::dlopenexception(std::string lib_name, std::string bonus) : m_msg(std::string("Could not load module ") + lib_name) {
	if (bonus != "")
		m_msg += "\n" + bonus;
}

const char* dlopenexception::what() const throw() {
	return m_msg.c_str();
}

dlsymexception::dlsymexception(std::string msg, std::string bonus) : m_msg(msg) {
	if (bonus != "")
		m_msg += "\n" + bonus;
}

const char* dlsymexception::what() const throw() {
	return m_msg.c_str();
}