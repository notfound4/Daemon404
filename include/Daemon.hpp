#pragma once

#include <boost/asio.hpp>
#include <map>
#include <string>

#include "Module.hpp"

class Daemon
{
private:
	static Daemon *instance;
	Daemon();
	~Daemon();
	Daemon(Daemon const&) = delete;
    Daemon& operator=(Daemon const&) = delete;

	boost::asio::io_service m_io_service;
	std::map<std::string, Module> m_modules;
public:
	static Daemon *Instance();

	int start();

	void stop();

	std::string handleMessage(const std::array<char, 1024> &data, std::size_t length);

	void addModule(std::string module_name);

	boost::program_options::options_description getOptions();

	void handleOptions(const boost::program_options::variables_map& vm);

	Module *getModule(std::string name);
};