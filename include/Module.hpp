#pragma once

#include "ModuleInterface.hpp"

#include <string>

class Module
{
public:
	Module(std::string lib_name);
	~Module();

	boost::program_options::options_description getOptions();

	std::string handleMessage(std::string msg);

	void handleOptions(const boost::program_options::variables_map& vm);

	ModuleInterface *get();
private:
	Module() = delete;
	Module(Module const&) = delete;
    Module& operator=(Module const&) = delete;

	void *m_lib;
	ModuleInterface *m_module;
	destroy_t* m_destroyer;
};