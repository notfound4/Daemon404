#pragma once

#include "ModuleInterface.hpp"

class ModuleI3 : public ModuleInterface {
public:
	ModuleI3();
	~ModuleI3();

	void handleI3(const std::array<char, 1024> &data, std::size_t length);
private:
};