#pragma once

#include <pulse/pulseaudio.h>
#include <string>

class Sink
{
public:
    uint32_t index;
    std::string name;
    std::string description;
    pa_cvolume volume;
    pa_volume_t volume_avg;
    int volume_percent;
    bool mute;

    Sink(const pa_sink_info* i);

private:
	void setVolume(const pa_cvolume* v);
};