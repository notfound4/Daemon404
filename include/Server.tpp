using boost::asio::local::stream_protocol;

template <class T>
Server<T>::Server(boost::asio::io_service& io_service, const std::string &file)
: m_acceptor(io_service, stream_protocol::endpoint(file)), m_socket(io_service)
{
	do_accept();
}

template <class T>
void Server<T>::do_accept()
{
	m_acceptor.async_accept(m_socket,
		[this](boost::system::error_code ec)
		{
			if (!ec)
			{
				std::make_shared<T>(std::move(m_socket))->start();
			}

			do_accept();
		});
}