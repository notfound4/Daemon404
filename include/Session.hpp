#pragma once

#include <boost/asio.hpp>
#include <array>
#include <string>

class Session : public std::enable_shared_from_this<Session>
{
public:
	Session(boost::asio::local::stream_protocol::socket sock);

	void start();

protected:
	virtual void do_read();

	virtual void do_write(std::string msg);

  	// The socket used to communicate with the client.
	boost::asio::local::stream_protocol::socket m_socket;

  	// Buffer used to store data received from the client.
	std::array<char, 1024> m_data;

private:
	Session() = delete;
	Session(Session const&) = delete;
    Session& operator=(Session const&) = delete;
};