#pragma once

#include <boost/asio.hpp>
#include <string>
#include <type_traits>

#include "Session.hpp"

template <class T>
class Server
{
	static_assert(std::is_base_of<Session,T>::value,
                  "Session class used by Server should inherit from 'Session'");
public:
	Server(boost::asio::io_service& io_service, const std::string &file);

private:
	Server() = delete;
	Server(Server const&) = delete;
    Server& operator=(Server const&) = delete;
    
	void do_accept();

	boost::asio::local::stream_protocol::acceptor m_acceptor;
	boost::asio::local::stream_protocol::socket m_socket;
};

#include "Server.tpp"